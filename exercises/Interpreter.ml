(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-20-27-32-37-39"]

type value =
  | IsNat of int
  | IsBool of bool

type exp =
  | Val of value
  | Eq of exp * exp
  | Plus of exp * exp
  | Ifte of exp * exp * exp

open Monads.Error

exception IncompatibleValues of string

let eq_val v1 v2 : value t =
  (match (v1,v2) with
   | (IsNat x,IsNat y) -> return (IsBool (x = y))
   | (IsBool x, IsBool y) -> return (IsBool (x = y))
   | _ -> err (IncompatibleValues "Eq"))

let plus_val v1 v2 : value t =
  (match (v1,v2) with
   | (IsNat x,IsNat y) -> return (IsNat (x + y))
   | _ -> err (IncompatibleValues "Plus"))

let ifte_val v1 e1 e2 : exp t =
  (match v1 with
   | IsBool v1 -> return (if v1 then e1 else e2)
   | _ -> err (IncompatibleValues "Ifte"))

let rec sem (e : exp) : value t =
  (match e with
   | Val x -> return x
   | Eq (e1,e2) -> (let* v1 = sem e1 in
                    (let* v2 = sem e2 in
                     eq_val v1 v2))
   | Plus (e1,e2) -> (let* v1 = sem e1 in
                      (let* v2 = sem e2 in
                       plus_val v1 v2))
   | Ifte (e1,e2,e3) -> (let* v1 = sem e1 in
                         (let* v2 = ifte_val v1 e2 e3 in
                          sem v2)))

(** * Tests *)

let%test _ = run (sem (Val (IsNat 42))) = IsNat 42

let%test _ = run (sem (Eq (Val (IsBool true), Val (IsBool true)))) = IsBool true

let%test _ = run (sem (Eq (Val (IsNat 3), Val (IsNat 3)))) = IsBool true

let%test _ =
  run (sem (Eq (Val (IsBool true), Val (IsBool false)))) = IsBool false

let%test _ = run (sem (Eq (Val (IsNat 42), Val (IsNat 3)))) = IsBool false

let%test _ = run (sem (Plus (Val (IsNat 42), Val (IsNat 3)))) = IsNat 45

let%test _ =
  run (sem (Ifte (Val (IsBool true), Val (IsNat 42), Val (IsNat 3)))) = IsNat 42

let%test _ =
  run (sem (Ifte (Val (IsBool false), Val (IsNat 42), Val (IsNat 3)))) = IsNat 3

let%test _ =
  run
    (sem
       (Ifte
          ( Eq (Val (IsNat 21), Plus (Val (IsNat 20), Val (IsNat 1)))
          , Val (IsNat 42)
          , Val (IsNat 3) )))
  = IsNat 42

(** ** Ill-typed expressions *)

let%test _ =
  try
    ignore(run (sem (Plus (Val (IsBool true), Val (IsNat 3)))));
    false
  with
    _ -> true

let%test _ =
  try
    ignore (run (sem (Ifte (Val (IsNat 3), Val (IsNat 42), Val (IsNat 44)))));
    false
  with
    _ -> true
