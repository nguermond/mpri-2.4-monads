(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Make (S: sig
                 type t
                 val init : t
               end) = struct

  module State = struct
    type 'a t = S.t -> 'a * S.t

    let return (a : 'a) : 'a t = fun s -> (a, s)

    let bind (m : 'a t) (f : 'a -> 'b t) : 'b t =
      (fun s ->
        (let (a,s') = m s in
         f a s'))
    let ( >>= ) = bind
    let ( let* ) = bind
  end

  module M = Monad.Expand (State)
  include M

  let get () : S.t t = fun s -> (s,s)

  let set (x : S.t) : unit t = fun s -> ((),x)

  let run (m : 'a t) : 'a = fst (m S.init)

end
