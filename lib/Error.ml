(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]


module Base = struct
  type 'a t =
    | Val of 'a
    | Err of exn

  let return a = Val a

  let bind m f =
    (match m with
     | Val a -> f a
     | Err e -> Err e)

  let ( >>= ) = bind

  let ( let* ) = bind
end

module M = Monad.Expand (Base)
include M
open Base


let err (e : exn) : 'a t = Err e

let try_with_finally (m : 'a t) (ks : 'a -> 'b t) (kf : exn -> 'b t) : 'b t =
  (match m with
   | Val a -> ks a
   | Err e -> kf e)

let run (m : 'a t) : 'a =
  (match m with
   | Val a -> a
   | Err e -> raise e)
